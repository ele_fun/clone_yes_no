import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { NgIconsModule } from '@ng-icons/core';

import { matfGitlabUncolored } from '@ng-icons/material-file-icons/uncolored';
import {
  MatButtonModule,
} from '@angular/material/button'
import {
  MatMenuModule,
} from '@angular/material/menu'
import {
  MatChipsModule,
} from '@angular/material/chips'


import {
  bootstrapYoutube,
  bootstrapInstagram,
  bootstrapTiktok,
  bootstrapLinkedin,
} from '@ng-icons/bootstrap-icons';

const routes: Routes = [];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    MatButtonModule,
    MatMenuModule,
    MatChipsModule,
    NgIconsModule.withIcons({
      matfGitlabUncolored,
      bootstrapYoutube,
      bootstrapInstagram,
      bootstrapTiktok,
      bootstrapLinkedin,
    }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
