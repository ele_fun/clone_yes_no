import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { tap } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'clone_yes_no';
  url = "http://192.168.18.71:3000/image/22";
  found: boolean = false;
  urlImage: string = "";

  constructor(
    private http: HttpClient,
  ) { }

  ngOnInit() {
    this.getImage()
  }


  getImage() {
    this.http.get(this.url)
      .pipe(
        tap(value => {
          console.log("bla bla bla")
          console.log({ value })
        })
      )
      .subscribe(
        {
          next: (response: any) => {
            console.log("response");
            console.log({ response });
            this.found = response.success;
            this.urlImage = response.item.url;
          },
          error: (err: any) => {
            console.log("error")
            this.found = false;
            console.log({ err });
          },
        }
      )

  }

}
